# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import scipy
import scipy.stats
import matplotlib.pyplot as plt
import sklearn.metrics as mt
#from sklearn.linear_model import LinearRegression
from sklearn import cross_validation
from sklearn.linear_model import LogisticRegression
from sklearn.lda import LDA
from sklearn.qda import QDA
#from sklearn.svm import SVC
from sklearn import neighbors
from sklearn.linear_model.perceptron import Perceptron
from sklearn.decomposition import PCA
from sklearn.svm import SVC




models = [LogisticRegression(), LDA(), QDA(), Perceptron(n_iter=68),neighbors.KNeighborsClassifier(n_neighbors=9,weights='distance'), SVC(kernel='rbf')]
models_names = ['LR', 'LDA', 'QDA', 'Perceptron','kNN', 'SVC-G']

def main():    
    X, t = load_data()
    plot_target_frequency(t)
    plot_accuracy_vs_folds(X, t)
    plot_accuracy_vs_components(X, t)
    plot_accuracy_between_model(X, t)
    plot_accuracy_vs_epochs_perceptron(X, t)
    plot_f2_between_model(X, t)
    plot_accuracy_vs_knn(X, t)
   
    
def load_data():
    # nome del file
    dataset_file_name = "wdbc.csv"
    
    # path del file
    path = "%s" % dataset_file_name
    #crea il data frame pandas a partire dai dati
    data = pd.read_csv(path, delimiter=',',header=None)
    
    # calcola dimensionalità delle features
    nfeatures = len(data.columns)-1
    
    # converti M e B rispettivamente con 1 e -1
    data = data.replace(['M'],[1]) 
    data = data.replace(['B'],[-1]) 
    
    # matrice delle features~\Documents\MLProject\mlproject\source
    X = np.asarray(data.iloc[:,0:nfeatures]).astype(float)
    #estrazioe dei valori target
    t = np.asarray(data.iloc[:,nfeatures:nfeatures+1]).astype(np.int8)
    
    return X, t

    
def cross_validate(model, X, t, folds, scoring = None):
    target = np.squeeze(t) 
    cv = cross_validation.StratifiedKFold(target, n_folds=folds)
    scores = cross_validation.cross_val_score(model, X, target, cv = cv, scoring = scoring)
    print "\n"
    print "************************ Accuracy con #folds: %s ************************" % folds
    print "\n"
    print scores   
    return {
        'mean': scores.mean(),
        'std': scores.std()
    }
    

def plot_target_frequency(t):
    target_frequency = scipy.stats.itemfreq(t)
    print target_frequency
    
    plt.figure()
    bar_width = 0.1
    opacity = 0.5
    left_edge =  1

    for n, row in enumerate(target_frequency):
        if row[0] < 0:
            label = 'Benigno'
            color = 'b'
        elif row[0] > 0:
            label = 'Maligno'
            color = 'r'
            left_edge += bar_width
        else:
            raise Exception("Unknown classification:", row[0])
        frequency = int(row[1])
        plt.bar(left=left_edge, 
                height=frequency,
                width = bar_width,
                alpha = opacity,
                color=color, 
                label=label,
                align = 'center')
    
    plt.gca().axes.xaxis.set_ticklabels([])
    plt.legend()
    plt.xlabel("Diagnosi")
    plt.ylabel("Frequenza")
    plt.title("Distribuzione dataset")
    plt.show()
    
def plot_accuracy_vs_folds(X, t):
    
   
    
    mean_accuracies = {}
    fold_range = range(2,21)
    
    for model, model_name in zip(models, models_names):
        print "\n***************************************************************************"
        print "Modello: %s" % model_name
        print "***************************************************************************"
        for folds in fold_range:
            scores = cross_validate(
                        model,
                        X, t,
                        folds
            )
            if not model_name in mean_accuracies.keys():
                mean_accuracies[model_name] = []
            mean_accuracies[model_name].append(scores['mean'])          
        
    plt.figure(facecolor='w')
    plt.grid(True) 
    for model_name in models_names:
        plt.plot(fold_range, mean_accuracies[model_name], label = model_name)
    plt.legend(loc='best')
    plt.title("Accuracy dei modelli per diversi fold")
    plt.ylim(0.8, 1)
    plt.show()
    
def plot_accuracy_vs_components(X, t):
    # calcola dimensionalità delle features
    nfeatures = X.shape[1]

    
    mean_accuracies = {}
    component_range = range(1,nfeatures+1)
    folds = 5
    
    for component in component_range:
        pca = PCA(n_components=component)

        X_reduct = pca.fit(X).transform(X)
    
        for model, model_name in zip(models, models_names):
            print "\n***************************************************************************"
            print "Modello: {0}, Componenti PCA {1}".format(model_name, component)
            print "***************************************************************************"
            
            scores = cross_validate(
                        model,
                        X_reduct, t,
                        folds
            )
            if not model_name in mean_accuracies.keys():
                mean_accuracies[model_name] = []
            mean_accuracies[model_name].append(scores['mean'])          
        
    plt.figure(facecolor='w')
    plt.grid(True) 
    for model_name in models_names:
        plt.plot(component_range, mean_accuracies[model_name], label = model_name)
    plt.legend(loc='best')
    plt.title("Accuracy dei modelli per diverse componenti")
    plt.ylim(0.8, 1)
    plt.ylabel('accuracy')
    plt.xlabel('# componenti PCA')
    plt.show()
           
def plot_accuracy_between_model(X, t):
    
    bar_width = 0.3
    opacity = 0.5
    color = 'b'
    plt.figure(facecolor='w')

    mean_accuracies = {}
    folds = 5
    
    
    
    for n, (model, model_name) in enumerate(zip(models, models_names)):
        print "\n***************************************************************************"
        print "Modello: %s" % model_name
        print "***************************************************************************"
        scores = cross_validate(
                        model,
                        X, t,
                        folds
        )
        if not model_name in mean_accuracies.keys():
            mean_accuracies[model_name] = []
        mean_accuracies[model_name].append(scores['mean']*100)
        plt.bar(left=n, 
            height=mean_accuracies[model_name],
            width = bar_width,
            alpha = opacity,
            label=model_name,
            color = color,
            align = 'center')
        
    plt.gca().axes.xaxis.set_ticklabels(models_names)
    plt.subplot().set_xticks(range(len(models)))
    plt.grid(True) 
    plt.title("Accuracy dei modelli con 5 fold")
    plt.ylabel('accuracy')
    plt.xlabel('modelli')
    plt.show()
    
def plot_f2_between_model(X, t):
    
    bar_width = 0.3
    opacity = 0.5
    color = 'r'
    plt.figure(facecolor='w')

    mean_accuracies = {}
    folds = 5
    
    
    
    for n, (model, model_name) in enumerate(zip(models, models_names)):
        print "\n***************************************************************************"
        print "Modello: %s" % model_name
        print "***************************************************************************"
        scores = cross_validate(
                        model,
                        X, t,
                        folds,
                        mt.make_scorer(mt.fbeta_score, beta=2)
        )
        if not model_name in mean_accuracies.keys():
            mean_accuracies[model_name] = []
        mean_accuracies[model_name].append(scores['mean']*100)
        plt.bar(left=n, 
            height=mean_accuracies[model_name],
            width = bar_width,
            alpha = opacity,
            label=model_name,
            color = color,
            align = 'center')
        
    plt.gca().axes.xaxis.set_ticklabels(models_names)
    plt.subplot().set_xticks(range(len(models)))
    plt.grid(True) 
    plt.title("F2 score dei modelli con 5 fold")
    plt.ylabel('f2')
    plt.xlabel('modelli')
    plt.show()    

def plot_accuracy_vs_epochs_perceptron(X, t):
    max_accuracy = None
    best_epochs = None
    
    epochs_range = range(1, 101)
    folds = 5
    
    mean_accuracies = []
    
    for epochs in epochs_range:
        scores = cross_validate(Perceptron(n_iter = epochs),
                                X, t,
                                folds
                                )
        print(
            "Accuracy Perceptron con %d iterazioni: %.1f%%" % (epochs, scores['mean']*100)
        )
        if scores > max_accuracy:
            max_accuracy = scores
            best_epochs = epochs
        
        mean_accuracies.append(scores['mean'])
        
    print(
        "Perceptron, numero di iterazioni migliore è: %d" % (best_epochs)
    )
    
    plt.figure(facecolor='w')
    plt.grid(True) 
    plt.plot(epochs_range, mean_accuracies, label = 'Perceptron')
    plt.legend(loc='best')
    plt.title("Accuracy di Perceptron per diverse iterazioni")
    plt.ylabel('accuracy')
    plt.xlabel('# iterazioni')
    plt.show()
 
    
def plot_accuracy_vs_knn(X, t):
   
    neighbors_numbers = range(1, 350)
    type_weights = ['uniform', 'distance'];
    folds = 5
    
    mean_accuracies = []
    
    plt.figure(facecolor='w')
    
    for weighting in type_weights:
        max_accuracy = None
        best_epochs = None
        mean_accuracies = []
        for neighbors_num in neighbors_numbers:
            scores = cross_validate(neighbors.KNeighborsClassifier(n_neighbors=neighbors_num,weights=weighting),
                                    X, t,
                                    folds
                                    )
           
            if scores > max_accuracy:
                max_accuracy = scores
                best_epochs = neighbors_num
            
            mean_accuracies.append(scores['mean'])
            
        print(
            "KNeighborsClassifier, numero di vicini migliore è: %d" % (best_epochs)
        )
        
        plt.grid(True) 
        plt.plot(neighbors_numbers, mean_accuracies, label = 'KNeighborsClassifier con peso '+weighting)
        plt.legend(loc='best')
        plt.title("Accuracy di KNeighborsClassifier per diversi numero di vicini")
        plt.ylabel('accuracy')
        plt.xlabel('# vicini')
        
        
    plt.show()         
    
main()