# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import math
import scipy
import scipy.stats
import matplotlib.pyplot as plt
import sklearn.metrics as mt
from sklearn import preprocessing
from sklearn import cross_validation
from sklearn import neighbors
from sklearn.linear_model import LogisticRegression
from sklearn.lda import LDA
from sklearn.qda import QDA
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model.perceptron import Perceptron
from sklearn.decomposition import PCA

models = [LogisticRegression(), LDA(), QDA(), Perceptron(n_iter=278),neighbors.KNeighborsClassifier(n_neighbors=9,weights='distance'), GaussianNB(), SVC(kernel='rbf'), SVC(kernel='linear')]
models_names = ['LR', 'LDA', 'QDA', 'Perceptron','kNN', 'NB', 'SVC-G', 'SVC-L']
colors = ['blue','red','green','orange','yellow','cyan','magenta', 'brown']

feature_dict = {i:label for i, label in zip(
            range(31),
              ('Raggio (M)',
              'Texture (M)',
              'Perimetro (M)',
              'Area (M)',
              'Smoothness (M)',
              'Compattezza (M)',
              'Concavita (M)',
              'Punti concavi (M)',
              'Simmetria (M)',
              'Dimensione Frattale (M)',
              'Raggio (SD)',
              'Texture (SD)',
              'Perimetro (SD)',
              'Area (SD)',
              'Smoothness (SD)',
              'Compattezza (SD)',
              'Concavita (SD)',
              'Punti concavi (SD)',
              'Simmetria (SD)',
              'Dimensione Frattale (SD)',
              'Raggio (W)',
              'Texture (W)',
              'Perimetro (W)',
              'Area (W)',
              'Smoothness (W)',
              'Compattezza (W)',
              'Concavita (W)',
              'Punti concavi (W)',
              'Simmetria (W)',
              'Dimensione Frattale (W)',
              ))}

def main():
    X, t = load_data()
    #plot_target_frequency(t)
    plot_features_distribution(X, t)
    #plot_accuracy_vs_folds(X, t)
    #plot_f2_vs_folds(X, t)
    #plot_accuracy_vs_components(X, t)
    #plot_accuracy_between_model(X, t)
    #plot_accuracy_vs_epochs_perceptron(X, t)
    #plot_f2_between_model(X, t)
    #plot_accuracy_vs_knn(X, t)
    
    #min_max_scaler = preprocessing.MinMaxScaler()
    #X_min_max = min_max_scaler.fit_transform(X)
    #
    #plot_accuracy_vs_folds(X_min_max, t)
    #plot_f2_vs_folds(X_min_max, t)
    #plot_variance_vs_components(X_min_max)
    #plot_accuracy_vs_components(X_min_max, t)
    #plot_accuracy_between_model(X_min_max, t)
    #plot_accuracy_vs_epochs_perceptron(X_min_max, t)
    #plot_accuracy_vs_knn(X_min_max, t)
    #plot_f2_between_model(X_min_max, t)
    
    std_scaler = preprocessing.StandardScaler().fit(X)
    X_std = std_scaler.transform(X)
    

    #plot_accuracy_vs_folds(X_std, t)
    #plot_f2_vs_folds(X_std, t)
    #plot_explained_variance(X_std)
    #plot_accuracy_vs_components(X_std, t)
    #plot_accuracy_between_model(X_std, t)
    #plot_accuracy_vs_epochs_perceptron(X_std, t)
    #plot_accuracy_vs_knn(X_std, t)
    #plot_f2_between_model(X_std, t)
    
    
def load_data():
    
    # path del file
    path = "wdbc.csv"
    #crea il data frame pandas a partire dai dati
    data = pd.read_csv(path, delimiter=',',header=None)
    
    # calcola dimensionalità delle features
    nfeatures = len(data.columns)-1
    
    # converti M e B rispettivamente con 1 e -1
    data = data.replace(['M'],[1]) 
    data = data.replace(['B'],[-1]) 
    
    # matrice delle features
    X = np.asarray(data.iloc[:,0:nfeatures]).astype(float)
    #estrazioe dei valori target
    t = np.asarray(data.iloc[:,nfeatures:nfeatures+1]).astype(np.int8)
    
    return X, t

    
def cross_validate(model, X, t, folds, scoring = None):
    target = np.squeeze(t) 
    cv = cross_validation.StratifiedKFold(target, n_folds=folds)
    scores = cross_validation.cross_val_score(model, X, target, cv = cv, scoring = scoring)
    print "\n"
    print "************************ Accuracy con #folds: %s ************************" % folds
    print "\n"
    print scores   
    return {
        'mean': scores.mean(),
        'std': scores.std()
    }
    

def plot_target_frequency(t):
    target_frequency = scipy.stats.itemfreq(t)
    print target_frequency
    
    plt.figure(facecolor='w')
    bar_width = 0.1
    opacity = 0.5
    left_edge =  1

    for n, row in enumerate(target_frequency):
        if row[0] < 0:
            label = 'Benigno'
            color = 'b'
        elif row[0] > 0:
            label = 'Maligno'
            color = 'r'
            left_edge += bar_width
        else:
            raise Exception("Unknown classification:", row[0])
        frequency = int(row[1])
        plt.bar(left=left_edge, 
                height=frequency,
                width = bar_width,
                alpha = opacity,
                color=color, 
                label=label,
                align = 'center')
    
    plt.gca().axes.xaxis.set_ticklabels([])
    plt.legend()
    plt.xlabel("Diagnosi")
    plt.ylabel("Frequenza")
    plt.title("Distribuzione dataset")
    plt.savefig('target_frequency.pdf',
                transparent=True)
    plt.show()
    
def plot_accuracy_vs_folds(X, t):
        
    mean_accuracies = {}
    fold_range = range(2,21)
    
    for model, model_name in zip(models, models_names):
        print "\n***************************************************************************"
        print "Modello: %s" % model_name
        print "***************************************************************************"
        for folds in fold_range:

            scores = cross_validate(
                        model,
                        X, t,
                        folds
            )
            if not model_name in mean_accuracies.keys():
                mean_accuracies[model_name] = []
            mean_accuracies[model_name].append(scores['mean']*100)          
        
    plt.figure(facecolor='w')
    plt.grid(True) 
    for model_name, model_color in zip(models_names, colors):
        plt.plot(fold_range, mean_accuracies[model_name], '.', linestyle='-', label = model_name, color = model_color)
    plt.legend(loc='best')
    #plt.title("Accuracy dei modelli per diversi fold")
    plt.ylim(80, 100)
    plt.xlabel('# fold')
    plt.ylabel('% accuracy')
    plt.savefig('acc_vs_folds.pdf',
                transparent=True)
    plt.show()
   
   
def plot_f2_vs_folds(X, t):
        
    mean_f2 = {}
    fold_range = range(2,21)
    
    for model, model_name in zip(models, models_names):
        print "\n***************************************************************************"
        print "Modello: %s" % model_name
        print "***************************************************************************"
        for folds in fold_range:

            scores = cross_validate(
                        model,
                        X, t,
                        folds,
                        mt.make_scorer(mt.fbeta_score, beta=2)
            )
            if not model_name in mean_f2.keys():
                mean_f2[model_name] = []
            mean_f2[model_name].append(scores['mean']*100)          
        
    plt.figure(facecolor='w')
    plt.grid(True) 
    for model_name in models_names:
        plt.plot(fold_range, mean_f2[model_name], '.', linestyle='-', label = model_name)
    plt.legend(loc='best')
    #plt.title("Accuracy dei modelli per diversi fold")
    plt.ylim(70, 100)
    plt.xlabel('# fold')
    plt.ylabel('% f2')
    plt.savefig('f2_vs_folds.pdf',
                transparent=True)
    plt.show()

    
def plot_explained_variance(X):
    
    # calcola dimensionalità delle features
    nfeatures = X.shape[1]
    #tickpos = [i for i in range(1, nfeatures+1)]    
    
    pca = PCA(n_components=nfeatures)
    X_reduct = pca.fit(X).transform(X)
    cum_var_exp = np.cumsum(pca.explained_variance_ratio_)
    print cum_var_exp
    print pca.explained_variance_ratio_
    
    plt.figure(facecolor = 'white')
    plt.bar(range(1, nfeatures+1), pca.explained_variance_ratio_, align='center', label = 'Explained Variance singola')
    plt.step(range(1, nfeatures+1), cum_var_exp, where = 'mid', label = 'Explained Variance cumulata')
    plt.axis('tight')
    plt.ylim(0.0, 1.2)
    plt.xlabel('Principal component')
    plt.ylabel('Explained variance ratio')
    # Cambiamo la dimensione dei tick sull'asse delle x
    #plt.tick_params(axis='x', which='major', labelsize=6)
    #plt.xticks(tickpos, tickpos)
    plt.legend(loc='best')
    
    plt.savefig('explained_variance_ratio.pdf',
                transparent=True)
    plt.show()
    
def plot_accuracy_vs_components(X, t):
    # calcola dimensionalità delle features
    nfeatures = X.shape[1]

    mean_accuracies = {}
    component_range = range(1,nfeatures+1)
    folds = 5

    for component in component_range:
        pca = PCA(n_components = component)
        X_reduct = pca.fit(X).transform(X)
        #std_scaler = preprocessing.StandardScaler().fit(X_reduct)
        #X_std = std_scaler.transform(X_reduct)
            
        for model, model_name in zip(models, models_names):
            print "\n***************************************************************************"
            print "Modello: {0}, Componenti PCA {1}".format(model_name, component)
            print "***************************************************************************"
            
            scores = cross_validate(
                        model,
                        X_reduct, t,
                        folds,
                    )
            if not model_name in mean_accuracies.keys():
                mean_accuracies[model_name] = []
            mean_accuracies[model_name].append(scores['mean']) 
            print "Accuracy media: %0.4f" % scores['mean']         
        
    plt.figure(facecolor='w')
    plt.grid(True) 
    for model_name, model_color in zip(models_names, colors):
        plt.plot(component_range, mean_accuracies[model_name], '.', linestyle='-', label = model_name, color = model_color)
    plt.legend(loc='best')
    plt.ylim(0.7, 1)
    plt.ylabel('accuracy')
    plt.xlabel('# componenti PCA')
    plt.savefig('acc_vs_components.pdf',
                transparent=True)
    plt.show()
           
def plot_accuracy_between_model(X, t):
    
    bar_width = 0.3
    opacity = 0.5
    color = 'b'
    plt.figure(facecolor='w')

    mean_accuracies = {}
    folds = 5
    
    for n, (model, model_name) in enumerate(zip(models, models_names)):
        print "\n***************************************************************************"
        print "Modello: %s" % model_name
        print "***************************************************************************"
        scores = cross_validate(
                        model,
                        X, t,
                        folds,
        )
        if not model_name in mean_accuracies.keys():
            mean_accuracies[model_name] = []
        mean_accuracies[model_name].append(scores['mean']*100)
        print "Accuracy media: %0.4f" % scores['mean']         
        plt.bar(left=n, 
            height=mean_accuracies[model_name],
            width = bar_width,
            alpha = opacity,
            label=model_name,
            color = color,
            align = 'center')
        
    plt.gca().axes.xaxis.set_ticklabels(models_names)
    plt.subplot().set_xticks(range(len(models)))
    plt.grid(True) 
    plt.title("Accuracy dei modelli con 5 fold")
    plt.ylabel('accuracy')
    plt.xlabel('modelli')
    plt.show()
    
def plot_f2_between_model(X, t):
    
    bar_width = 0.3
    opacity = 0.5
    color = 'r'
    plt.figure(facecolor='w')

    mean_accuracies = {}
    folds = 5
    
    for n, (model, model_name) in enumerate(zip(models, models_names)):
        print "\n***************************************************************************"
        print "Modello: %s" % model_name
        print "***************************************************************************"
        scores = cross_validate(
                        model,
                        X, t,
                        folds,
                        mt.make_scorer(mt.fbeta_score, beta=2)
        )
        if not model_name in mean_accuracies.keys():
            mean_accuracies[model_name] = []
        mean_accuracies[model_name].append(scores['mean']*100)
        print "F2 Score media: %0.4f" % scores['mean']
        plt.bar(left=n, 
            height=mean_accuracies[model_name],
            width = bar_width,
            alpha = opacity,
            label=model_name,
            color = color,
            align = 'center')
        
    plt.gca().axes.xaxis.set_ticklabels(models_names)
    plt.subplot().set_xticks(range(len(models)))
    plt.grid(True) 
    plt.title("F2 score dei modelli con 5 fold")
    plt.ylabel('f2')
    plt.xlabel('modelli')
    plt.show()    

def plot_accuracy_vs_epochs_perceptron(X, t):
    max_accuracy = None
    best_epochs = None
    
    epochs_range = range(1, 1001)
    folds = 5
    
    mean_accuracies = []
    
    for epochs in epochs_range:
        scores = cross_validate(Perceptron(n_iter = epochs),
                                preprocessing.MinMaxScaler().fit_transform(X), t,
                                folds,                               
                                )
        print(
            "Accuracy Perceptron con %d iterazioni: %.1f%%" % (epochs, scores['mean']*100)
        )
        if scores > max_accuracy:
            max_accuracy = scores
            best_epochs = epochs
        
        mean_accuracies.append(scores['mean'])
        
    print(
        "Perceptron, numero di iterazioni migliore è: %d" % (best_epochs)
    )
    
    plt.figure(facecolor='w')
    plt.grid(True) 
    plt.plot(epochs_range, mean_accuracies, label = 'Perceptron')
    plt.legend(loc='best')
    plt.ylabel('accuracy')
    plt.xlabel('# iterazioni')
    plt.savefig('perceptron_vs_epochs_2.pdf',
                transparent = True)
    plt.show()
    
def plot_accuracy_vs_knn(X, t):
   
    neighbors_numbers = range(1, 350)
    type_weights = ['uniform', 'distance'];
    folds = 5
    
    mean_accuracies = []
    
    plt.figure(facecolor='w')
    
    for weighting in type_weights:
        max_accuracy = None
        best_epochs = None
        mean_accuracies = []
        for neighbors_num in neighbors_numbers:
            scores = cross_validate(neighbors.KNeighborsClassifier(n_neighbors=neighbors_num,weights=weighting),
                                    X, t,
                                    folds
                                    )
           
            if scores > max_accuracy:
                max_accuracy = scores
                best_epochs = neighbors_num
            
            mean_accuracies.append(scores['mean'])
            
        print(
            "KNN, numero di vicini migliore è: %d" % (best_epochs)
        )
        
        plt.grid(True) 
        plt.plot(neighbors_numbers, mean_accuracies, label = 'KNN con peso '+weighting)
        plt.legend(loc='best')
        plt.title("Accuracy di KNN per diversi numero di vicini")
        plt.ylabel('accuracy')
        plt.xlabel('# vicini')
        
        
    plt.show()     
    
def plot_features_distribution(X, t): 
    t = t.ravel()
    
    nrows = 6
    ncols = 5
    fig, axes = plt.subplots(nrows = nrows, ncols = ncols, figsize = (25,15), subplot_kw={'xticks': [], 'yticks': []}, facecolor = 'w')
    label_dict = {-1: 'Benigno', 1: 'Maligno'}
    
    for ax, cnt in zip(axes.ravel(), range(31)):
        # setta dimensione bin
        min_b = math.floor(np.min(X[:,cnt]))
        max_b = math.ceil(np.max(X[:,cnt]))
        bins = np.linspace(min_b, max_b, 200)

        # plot degli istogrammi
        class_cancer = np.array([-1, 1])
        for lab, col in zip(class_cancer, ('blue', 'red')):
            ax.hist(X[t==lab, cnt],
                    color=col,
                    label='%s' % label_dict[lab],
                    bins=bins,
                    alpha=0.5)
        ylims = ax.get_ylim()
        fig.subplots_adjust(hspace=0.3, wspace=0.05)
        # nascondi ticks degli assi
        ax.tick_params(axis="both", which="both", bottom="off", top="off",
                labelbottom="on", left="off", right="off", labelleft="on")
    
        # plot annotation
        leg = ax.legend(loc='best', fancybox=True, fontsize=8)
        leg.get_frame().set_alpha(0.5)
        ax.set_ylim([0, max(ylims)+2])
        ax.set_xlabel(feature_dict[cnt])
        #ax.set_title('WDBC #%0.1f' % (cnt+1))
    
        # rimuove axis spines
        ax.spines["top"].set_visible(False)
        ax.spines["right"].set_visible(False)
        ax.spines["bottom"].set_visible(False)
        ax.spines["left"].set_visible(False)
    
    for nrow in range(nrows):
        axes[nrow][0].set_ylabel('#')    

    fig.tight_layout()
    plt.savefig('class_distribution.pdf',
                transparent = True)
    plt.show()
    
main()